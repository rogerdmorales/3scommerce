import { Routes } from '@angular/router'

import { HomeComponent } from './home/home.component'
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component'
import { CadastroProdutoComponent } from './cadastro-produto/cadastro-produto.component'
import {  CarrinhoComprasComponent } from './carrinho-compras/carrinho-compras.component'

export const ROUTES: Routes = [
    { path: '', component: HomeComponent },
    { path: 'cadastro-usuario', component: CadastroUsuarioComponent },
    { path: 'cadastro-produto', component: CadastroProdutoComponent },
    { path: 'carrinho-compras', component: CarrinhoComprasComponent }
]
