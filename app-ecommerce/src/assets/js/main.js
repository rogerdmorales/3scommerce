$(document).ready(function(){
	var windowWidth = window.innerWidth; //saber o valor da largura

    //efeito de focus para label e input
	$(".input-login").focusin(function(){
		$(this).addClass("focus");
		$(this).prev().addClass("focus");
	});
    //tirar o efeito da label e input
    $(".input-login").focusout(function(){
    	$(this).removeClass("focus");
    	$(this).prev().removeClass("focus");
    });
	//ABRIR E FECHAR O BOX DE USUARIO
	$("#btn-user").on("click", function() {
		$(".login").toggleClass("none");
	});

	$("#btn-user").on("click", function() {
		$(".btn-op").toggleClass("none");
	});

  $(".f-opcoes").on("click", function(){
    $(".login").toggleClass("none");
  });

	//ABRIR A CAIXA DE INPUT COM FOCUS - apenas para tamanho menor de 550px
	$(".btn-busca").on("click",function(){
		if (window.innerWidth <=550){
			var validarFocus = "Focus";
			$(".btn-busca").addClass(validarFocus);

			if ($(".input-busca").hasClass(validarFocus)){
				$(".input-busca").blur();
			} else {
				abrirInput();//função responsavel por abrir o input de busca
			}
		}
	});

	$(".fechar").on("click", function () {
		 fechaInput(); //função responsavel por fechar o input de busca
	});



	//função responsavel por fechar o input de busca
	function fechaInput() {
		$(".input-busca").removeClass("input-focus");
		$(".nav-left").css("display","flex");
		$(".logo").css("display","block");
		$(".input-busca").animate({ width:"toggle"});
		$(".fechar").hide();
	}
	//função responsavel por abrir o input de busca
	function abrirInput() {
		$(".nav-left , .logo").css("display","none");
		$(".input-busca").addClass("input-focus");
		$(".input-busca , .fechar").animate({ width:"toggle"});
		$(".fechar").show();
		$(".input-busca").focus();
	}
});
