package br.com.fatec.ecommerce.utils;

import java.io.File;
import java.io.IOException;
import java.util.Base64;

import org.apache.commons.io.FileUtils;

public class Utils {

	public static void decodeBase64StringToFile(String imageTitle, String imagePath, String base64String) throws IOException {
		File outputFile = new File(imagePath + imageTitle);

		// decode the string and write to file
		byte[] decodedBytes = Base64.getDecoder().decode(base64String);
		FileUtils.writeByteArrayToFile(outputFile, decodedBytes);
	}

	public static String encodeFileToBase64String(String imageTitle, String imagePath) throws IOException {
		File inputFile = new File(imagePath + imageTitle);

		byte[] fileContent = FileUtils.readFileToByteArray(inputFile);
		String encodedString = Base64.getEncoder().encodeToString(fileContent);

		return encodedString;
	}

}
