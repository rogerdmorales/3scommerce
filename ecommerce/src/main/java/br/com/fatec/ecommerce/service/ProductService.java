package br.com.fatec.ecommerce.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fatec.ecommerce.entity.Image;
import br.com.fatec.ecommerce.entity.Product;
import br.com.fatec.ecommerce.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository repository;
	
	@Autowired
	private ImageService imgService;
	
	public Product save(Product product) throws IOException {
		imgService.saveImages(product.getImages(), product);
		return repository.save(product);
	}
	
	public List<Product> findAll() throws IOException {
		List<Product> products = repository.findAll();
		
		for (Product product: products) {
			List<Image> images = imgService.getImagesFromFiles(product.getImages());
			product.setImages(images);
		}
		
		return products;
	}
	
	public Product findById(long id) throws IOException {
		Product product = repository.findById(id).get();
		List<Image> images = imgService.getImagesFromFiles(product.getImages());
		product.setImages(images);
		return product;
	}
}
