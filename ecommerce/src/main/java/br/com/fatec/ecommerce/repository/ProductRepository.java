package br.com.fatec.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fatec.ecommerce.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	public List<Product> findByDescription(String description);
}
