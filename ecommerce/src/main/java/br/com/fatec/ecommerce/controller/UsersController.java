package br.com.fatec.ecommerce.controller;


import br.com.fatec.ecommerce.entity.User;
import br.com.fatec.ecommerce.entity.request.UserRequest;
import br.com.fatec.ecommerce.entity.response.Response;
import br.com.fatec.ecommerce.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping( "/api/users" )
public class UsersController {

    @Autowired
    private UserService userService;

    @PostMapping
    public Response< UserRequest > addUser( @Valid @RequestBody UserRequest userRequest ) {
        User user = userService.save( new User( userRequest.getName(), userRequest.getEmail(), userRequest.getPhoneNumber(),
                userRequest.getPassword() ) );
        return new Response<>( userRequest );
    }

}
