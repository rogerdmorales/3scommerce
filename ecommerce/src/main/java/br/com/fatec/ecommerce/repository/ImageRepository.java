package br.com.fatec.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fatec.ecommerce.entity.Image;

public interface ImageRepository extends JpaRepository<Image, Long>{

}
