package br.com.fatec.ecommerce.service;

import br.com.fatec.ecommerce.entity.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.fatec.ecommerce.entity.User;
import br.com.fatec.ecommerce.repository.UserRepository;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@Service
public class UserService {
	
	@Autowired
	private UserRepository repository;
	
	public User save(User user) {

		if(emailExists(user.getEmail())) {
			throw new UsernameNotFoundException("E-mail address already in use");
		}

		User user = new User(userRequest.getName(), userRequest.getEmail(), userRequest.getPhoneNumber(),
				userRequest.getPassword());

		user.setPassword(passwordEncoder.encode(userRequest.getPassword()));

		User result = userService.save(user);

		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/api/users/{email}")
				.buildAndExpand(result.getEmail()).toUri();

		return repository.save(user);
	}
	
	public User findUserByEmail(String email) {
		return repository.findUserByEmail(email);
	}
	
	public boolean emailExists(String email) {
		return repository.existsByEmail(email);
	}
	
}
