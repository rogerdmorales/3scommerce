package br.com.fatec.ecommerce.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.fatec.ecommerce.entity.Product;
import br.com.fatec.ecommerce.entity.response.ApiResponse;
import br.com.fatec.ecommerce.service.ProductService;

@RestController
@RequestMapping("api/products")
public class ProductsController extends AbstractController{
	
	@Autowired
	private ProductService service;
	
	@PostMapping(consumes=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> add(@RequestBody Product product) {
		product.setUser(getLoggedUser());
		try {
			
			return ResponseEntity.ok(service.save(product)); 
		} catch (IOException e) {
			return new ResponseEntity<ApiResponse>(new ApiResponse(false, "Sorry, couldn't save the images" ), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Product> getProducts() {
		try {
			return service.findAll();
		} catch (IOException e) {
			return null;
		}
	}
	
	@GetMapping("/id/{id}")
	public Product getProductById(@PathVariable long id) {
		try {
			return service.findById(id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
