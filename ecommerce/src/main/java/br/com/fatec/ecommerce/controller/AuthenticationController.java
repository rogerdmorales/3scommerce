package br.com.fatec.ecommerce.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.fatec.ecommerce.entity.User;
import br.com.fatec.ecommerce.entity.request.LoginRequest;
import br.com.fatec.ecommerce.entity.request.UserRequest;
import br.com.fatec.ecommerce.entity.response.ApiResponse;
import br.com.fatec.ecommerce.entity.response.JwtAuthenticationResponse;
import br.com.fatec.ecommerce.security.JwtTokenProvider;
import br.com.fatec.ecommerce.security.UserPrincipal;
import br.com.fatec.ecommerce.service.UserService;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController extends AbstractController{
	@Autowired
	private UserService userService;

	@Autowired
    AuthenticationManager authenticationManager;

	@Autowired
    PasswordEncoder passwordEncoder;

	@Autowired
    JwtTokenProvider tokenProvider;

    @Value("${app.jwtExpirationInMs}")
    private long jwtExpirationInMs;

	@PostMapping("/login")
	public ResponseEntity<?> authenticate(@RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        User user = userService.findUserByEmail(loginRequest.getEmail());
        UserPrincipal userResponse = UserPrincipal.create(user);
        
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, jwtExpirationInMs, userResponse));
	}
	
	@PostMapping("/signup")
	public ResponseEntity<?> addUser(@Valid @RequestBody UserRequest userRequest) {

        if(userService.emailExists(userRequest.getEmail())) {
            return new ResponseEntity<ApiResponse>(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }
		
		User user = new User(userRequest.getName(), userRequest.getEmail(), userRequest.getPhoneNumber(),
				userRequest.getPassword());
		
		user.setPassword(passwordEncoder.encode(userRequest.getPassword()));

		User result = userService.save(user);
		
		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
	}

}
