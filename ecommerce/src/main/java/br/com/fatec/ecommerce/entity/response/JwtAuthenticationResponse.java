package br.com.fatec.ecommerce.entity.response;

import br.com.fatec.ecommerce.security.UserPrincipal;

public class JwtAuthenticationResponse {
    private String accessToken;
    private long expiresIn;
    private String tokenType = "Bearer";
    private UserPrincipal user;

    public JwtAuthenticationResponse(String accessToken, long expiresIn, UserPrincipal user) {
        this.accessToken = accessToken;
        this.setExpiresIn(expiresIn);
        this.user = user;
    }
    
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

	public long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(long expiresIn) {
		this.expiresIn = expiresIn;
	}
	
	public UserPrincipal getUser() {
		return user;
	}
	
	public void setUser(UserPrincipal user) {
		this.user = user;
	}

	
}