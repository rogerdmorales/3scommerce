package br.com.fatec.ecommerce.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.fatec.ecommerce.entity.Image;
import br.com.fatec.ecommerce.entity.Product;
import br.com.fatec.ecommerce.repository.ImageRepository;
import br.com.fatec.ecommerce.utils.Utils;

@Service
public class ImageService {
	
	@Autowired
	private ImageRepository repository;
	
	@Value("${app.imagesPath}")
    private String imagesPath;
	
	public void saveImage(Image image) throws IOException {
		Utils.decodeBase64StringToFile(image.getTitle(), imagesPath, image.getBase64Img());
	}

	public void saveImages(List<Image> images, Product product) throws IOException {
		repository.saveAll(images);
		
		for (Image image : images) {
			image.setProduct(product);
			saveImage(image);
		}
		
	}
	
	public List<Image> getImagesFromFiles (List<Image> images) throws IOException {
		
		for (Image image: images) {
			String imgStr = Utils.encodeFileToBase64String(image.getTitle(), imagesPath);
			image.setBase64Img(imgStr);
		}
		
		return images;
	}

}
