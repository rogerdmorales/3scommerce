package br.com.fatec.ecommerce.entity.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Objects;

@JsonInclude( JsonInclude.Include.NON_NULL )
public class Response< T > {

    private T body;

    public Response( T body ) { this.body = body; }

    @Override
    public String toString() {
        return "Response{" +
                "body=" + body +
                '}';
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) return true;
        if( o == null || getClass() != o.getClass() ) return false;
        Response< ? > response = ( Response< ? > ) o;
        return body.equals( response.body );
    }

    @Override
    public int hashCode() {
        return Objects.hash( body );
    }
}
