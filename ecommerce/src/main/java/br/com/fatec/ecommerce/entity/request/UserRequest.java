package br.com.fatec.ecommerce.entity.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserRequest {
	
	@NotBlank
	@Size(min = 4, max = 40)
	private String name;
	
	@NotBlank
    @Size(max = 40)
    @Email
	private String email;
	
	@NotBlank
	@Size(min = 8, max = 9)
	private String phoneNumber;
	
	@NotBlank
    @Size(min = 6, max = 20)
	private String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	

}
