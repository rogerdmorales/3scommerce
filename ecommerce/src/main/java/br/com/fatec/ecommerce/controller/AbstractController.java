package br.com.fatec.ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.fatec.ecommerce.entity.User;
import br.com.fatec.ecommerce.security.UserPrincipal;
import br.com.fatec.ecommerce.service.UserService;

public abstract class AbstractController {
	
	@Autowired
	private UserService usuarioService;
	
	public User getLoggedUser() {
		UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = usuarioService.findUserByEmail(userPrincipal.getEmail());
		
		return user;
	}
}
